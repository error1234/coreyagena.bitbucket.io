# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 15:51:57 2021

@author: melab15
"""
fiblist = [0] * 10000               # Create a list of 10000 zeros
fiblist[1] = 1
def fib (idx):
    '''
    @brief    This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired Fibonacci number
    '''
    if idx == 0 or idx == 1:        # Fill the 0 and 1 positions of the list
        return fiblist[idx]
    else:
        n = 2                       # Fill list starting from index 2
        x = 0
        while n <= idx:
            x = fiblist[n-1] + fiblist[n-2]     # The fibonacci equation
            fiblist[n] = x()                    # Add to the list
            n += 1
        return x

def main():
    while True:
        my_string = input('Please enter an index: ')
        try:
            idx = int(my_string)
        except:                     # Return error if idx is not a integer
            print('The index must be an integer!')
            continue
        else:
            if idx < 0:             # Return error if idx is negative
                print('The index must be nonegative!')
                continue
            else:
                fib(idx)            # Return the Fibonacci number
                print ('Fibonacci number at '
                       'index {:} is {:}.'.format(idx,fib(idx)))
                my_string1 = 'wrong'
                while my_string1 != 'q' or '':
                    my_string1 = \              # Prompt to exit or continue
                        input('Enter q to quit or press enter to continue: ')
                    if my_string1 == 'q':
                        return
                    elif my_string1 == '':
                        break
                    else:           # If the input is invalid return a error
                        print('Invalid input!')
                        continue

if __name__ == '__main__':
    main()

    